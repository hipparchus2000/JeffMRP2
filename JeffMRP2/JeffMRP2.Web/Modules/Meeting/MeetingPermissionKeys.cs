﻿
namespace JeffMRP2.Meeting
{
    public class PermissionKeys
    {
        public const string General = "Meeting:General";
        public const string Management = "Meeting:Management";
    }
}

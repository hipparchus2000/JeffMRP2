﻿using Serenity.Services;

namespace JeffMRP2.Northwind
{
    public class OrderListRequest : ListRequest
    {
        public int? ProductID { get; set; }
    }
}
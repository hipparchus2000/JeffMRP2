﻿namespace JeffMRP2.Common {
    export interface UserPreferenceRetrieveResponse extends Serenity.ServiceResponse {
        Value?: string;
    }
}


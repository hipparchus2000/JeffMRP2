﻿namespace JeffMRP2.Northwind {
    export interface OrderListRequest extends Serenity.ListRequest {
        ProductID?: number;
    }
}


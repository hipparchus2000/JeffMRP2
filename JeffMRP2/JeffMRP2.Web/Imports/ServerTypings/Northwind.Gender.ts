﻿namespace JeffMRP2.Northwind {
    export enum Gender {
        Male = 1,
        Female = 2
    }
    Serenity.Decorators.registerEnum(Gender, 'JeffMRP2.Northwind.Entities.Gender');
}

